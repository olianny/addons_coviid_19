# -*- coding: utf-8 -*-

from odoo import models, fields, api

class covid_19(models.Model):
	_name = 'covid_19.covid_19'

	source = fields.Char(string="source", required=True)
	date= fields.Datetime(string='Date Time ', required=True, default = fields.Datetime.now ())
	country_id = fields.Many2one('res.country', required=True)
	infected  = fields.Integer(string='Infectados', required=True, default=0)
	recovered = fields.Integer(string='Recuperados', required=True, default=0) 
	deceased  = fields.Integer(string='Muertos', required=True, default=0)
	total_infected  = fields.Integer(string='total_Infected', compute='set_total_infected', required=True, default=0)
	total_recovered  = fields.Integer(string='total_Recovered', compute='set_total_recovered', required=True, default=0)
	total_deceased  = fields.Integer(string='total_Deceased', compute='set_total_deceased', required=True, default=0)


	def set_total_infected (self):
		for data in self:
			domain=[
					('country_id','=',data.country_id.id),
					('date','<',data.date)
				    ]
			records=self.search(domain)
			infecteds=records.mapped('infected')
			data.total_infected=sum(infecteds)+data.infected

	def set_total_recovered (self):
		for data in self:
			domain=[
					('country_id','=',data.country_id.id),
					('date','<',data.date)
				    ]
			records=self.search(domain)
			recovereds=records.mapped('recovered')
			data.total_recovered=sum(recovereds)+data.recovered

	def set_total_deceased (self):
		for data in self:
			domain=[
					('country_id','=',data.country_id.id),
					('date','<',data.date)
				    ]
			records=self.search(domain)
			deceaseds=records.mapped('deceased')
			data.total_deceased=sum(deceaseds)+data.deceased
